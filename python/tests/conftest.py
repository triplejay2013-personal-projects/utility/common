import sys
from io import StringIO
from typing import Any, Iterator

import pytest
from aioresponses import aioresponses

from common.logging import setup_logging

setup_logging()


@pytest.fixture
def aioresponse_mock() -> Iterator[Any]:
    with aioresponses() as m:
        yield m


class Capturing(list):
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self

    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        del self._stringio
        sys.stdout = self._stdout
