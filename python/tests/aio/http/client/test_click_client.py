# type: ignore
# flake8: noqa
"""Test sample click client."""
from http import HTTPStatus

import click
import click_client
import pytest
from click.testing import CliRunner
from yarl import URL


@pytest.fixture
def runner():
    # Setup
    yield CliRunner()
    # Teardown


HOST = URL("http://localhost:8080/api")
POST = {"id": 1, "owner": "me", "editor": "you", "title": "zot"}
POST_WITH_OPT = {"id": 1, "owner": "me", "editor": "you", "title": "zot", "text": "thetext"}


@pytest.fixture
def mock_server(aioresponse_mock):
    # Setup
    aioresponse_mock.get(HOST / "1", status=HTTPStatus.OK, payload={"data": POST})  # Get
    aioresponse_mock.put(HOST / "1", status=HTTPStatus.OK, payload={"data": POST_WITH_OPT})
    aioresponse_mock.patch(HOST / "1", status=HTTPStatus.OK, payload={"data": POST_WITH_OPT})
    aioresponse_mock.post(HOST, status=HTTPStatus.CREATED, payload={"data": POST_WITH_OPT})
    aioresponse_mock.delete(HOST / "1", status=HTTPStatus.OK)

    yield aioresponse_mock

    # Teardown


class TestPostDataclass:
    def test_instantiate_class_no_optional(self):
        click_client.Post(1, "zot", "bar", "foo")

    def test_instantiate_class_with_optional(self):
        click_client.Post(1, "zot", "bar", "foo", "Text")


class TestClickClient:
    """Test sample Click client."""

    def test_click_main(self, runner, mock_server):
        result = runner.invoke(click_client.main, catch_exceptions=False)
        assert result.exit_code == 0
        assert "Usage: main [OPTIONS] COMMAND [ARGS]" in result.output

    @pytest.mark.skip("Something is wrong with underlying dataclass `Post`")
    def test_click_get(self, runner, mock_server):
        result = runner.invoke(click_client.main, args=["get", "1"], catch_exceptions=False)
        assert result.exit_code == 0
        assert "Owner" in result.output
        assert "Editor" in result.output

    @pytest.mark.skip("Something is wrong with underlying dataclass `Post`")
    def test_click_put(self, runner, mock_server):
        result = runner.invoke(
            click_client.main, args=["put", "1", "--title", "zot", "--text", "thetext"], catch_exceptions=False
        )
        assert result.exit_code == 0
        assert "Owner" in result.output
        assert "Editor" in result.output
        assert "Text" in result.output

    def test_click_delete(self, runner, mock_server):
        result = runner.invoke(click_client.main, args=["delete", "1"], catch_exceptions=False)
        assert result.exit_code == 0
        # TODO

    @pytest.mark.skip("Something is wrong with underlying dataclass `Post`")
    def test_click_create(self, runner, mock_server):
        result = runner.invoke(
            click_client.main, args=["create", "--title", "zot", "--text", "thetext"], catch_exceptions=False
        )
        assert result.exit_code == 0
        assert "Owner" in result.output
        assert "Editor" in result.output
        assert "Text" in result.output

    def test_click_show_traceback(self, runner):
        result = runner.invoke(click_client.main, args=["--show-traceback", "fail"])
        assert result.exit_code == 1
        assert not result.output

    def test_click_show_no_traceback(self, runner):
        result = runner.invoke(click_client.main, args=["fail"])
        assert result.exit_code == 0
        assert "Error" in result.output
