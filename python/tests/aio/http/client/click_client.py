# type: ignore
# flake8: noqa
"""Perform async web operations.

Provides cmd-line interface/plugin

This follows the example found here ---v
https://us-pycon-2019-tutorial.readthedocs.io/aiohttp_client_full.html#full-client

This is an example of how to utilize the common.aio.http.client.Client class and it's constituents
"""
import asyncio  # async calls
import functools  # Wrap func for click calls
from contextlib import asynccontextmanager  # Form context-capable methods
from dataclasses import dataclass, field  # Succinct classes (Read-only in our case)
from types import TracebackType  # Provides more use-specific 'builtin' types
from typing import (  # Type hinting
    Any,
    AsyncIterator,
    Awaitable,
    Callable,
    List,
    Optional,
    Type,
)

import aiohttp  # Async http calls
import click  # Provide cmd-line interface for our client calls
from yarl import URL  # Immutable URL with accessible properties

from common.aio.http.client import Client, Dataclass, Root, pass_root

# Using ellipsis in Typing calls --v
# https://www.geeksforgeeks.org/what-is-three-dots-or-ellipsis-in-python3/


# Blog post api example


@dataclass(frozen=True)
class Post(Dataclass):
    id: int
    owner: str
    editor: str
    title: str
    text: Optional[str] = None

    def pprint(self) -> None:
        click.echo(f"Post: {self.id}")
        click.echo(f"\tOwner: {self.owner}")
        click.echo(f"\tEditor: {self.editor}")
        click.echo(f"\tTitle: {self.title}")
        if self.text is not None:
            click.echo(f"\tText: {self.text}")


class ExampleClient(Client):
    def __init__(self, base_url: URL, user: str) -> None:
        super().__init__(base_url, dataclass=Post)
        self._user = user

    async def post(self, title: str, text: str) -> Post:
        return await super().post(endpoint="api", json={"owner": self._user, "title": title, "text": text})

    async def get(self, post_id: int) -> Post:
        return await super().get(endpoint=f"api/{post_id}")

    async def delete(self, post_id: int) -> None:
        await super().delete(endpoint="api/{post_id}")

    async def patch(self, post_id: int, title: Optional[str] = None, text: Optional[str] = None) -> Post:
        json = {"editor": self._user}
        if title is not None:
            json["title"] = title
        if text is not None:
            json["text"] = text
        return await super().patch(endpoint=f"api/{post_id}", json=json)

    async def put(self, post_id: int, title: Optional[str] = None, text: Optional[str] = None) -> Post:
        json = {"editor": self._user}
        if title is not None:
            json["title"] = title
        if text is not None:
            json["text"] = text
        return await super().put(endpoint=f"api/{post_id}", json=json)

    async def list(self) -> List[Post]:
        return await super().list(endpoint="api")

    async def fail(self) -> None:
        raise Exception


@click.group()
@click.option("--base-url", type=str, default="http://localhost:8080", show_default=True)
@click.option("--user", type=str, default="Anonymous", show_default=True)
@click.option("--show-traceback", is_flag=True, default=False, show_default=True)
@click.pass_context
def main(ctx: click.Context, base_url: str, user: str, show_traceback: bool) -> None:
    ctx.obj = Root(URL(base_url), show_traceback, _client=ExampleClient, meta={"user": user})


@main.command()
@click.option("--title", type=str, required=True)
@click.option("--text", type=str, required=True)
@pass_root
async def create(root: Root, title: str, text: str) -> None:
    """Create new blog post."""
    async with root.client(user=root.meta["user"]) as client:
        post = await client.post(title, text)
        click.echo(f"Created post {post.id}")
        post.pprint()


@main.command()
@click.argument("post_id", type=int)
@pass_root
async def get(root: Root, post_id: int) -> None:
    """Get detailed info about post."""
    async with root.client(user=root.meta["user"]) as client:
        post = await client.get(post_id)
        post.pprint()


@main.command()
@click.argument("post_id", type=int)
@click.option("--text", type=str)
@click.option("--title", type=str)
@pass_root
async def put(root: Root, post_id: int, title: Optional[str], text: Optional[str]) -> None:
    """put existing post."""
    async with root.client(user=root.meta["user"]) as client:
        post = await client.put(post_id, title, text)
        post.pprint()


@main.command()
@click.argument("post_id", type=int)
@click.option("--text", type=str)
@click.option("--title", type=str)
@pass_root
async def patch(root: Root, post_id: int, title: Optional[str], text: Optional[str]) -> None:
    """patch existing post."""
    async with root.client(user=root.meta["user"]) as client:
        post = await client.patch(post_id, title, text)
        post.pprint()


@main.command()
@click.argument("post_id", type=int)
@pass_root
async def delete(root: Root, post_id: int) -> None:
    """Delete existing post."""
    async with root.client(user=root.meta["user"]) as client:
        await client.delete(post_id)
        click.echo(f"Deleted post - id: {post_id}")


@main.command()
@pass_root
async def list(root: Root) -> None:
    """List existing posts."""
    async with root.client(user=root.meta["user"]) as client:
        posts = await client.list()
        click.echo("List posts:")
        for post in posts:
            post.pprint()


@main.command()
@pass_root
async def fail(root: Root) -> None:
    """Just raise an exception."""
    async with root.client(user=root.meta["user"]) as client:
        await client.fail()


if __name__ == "__main__":
    main()
