# type: ignore
# flake8: noqa
"""Test base async client methods and dataclasses."""
import asyncio
from dataclasses import dataclass, field
from typing import Any, Dict, List, Union

import pytest
from conftest import Capturing
from yarl import URL

from common.aio.http.client import Client, Dataclass, Root

HOST = "http://test.com"


@pytest.fixture()
async def root():
    # Setup
    yield Root(URL(HOST), True)
    # Teardown


class TestRootDataclass:
    """Test Root client."""

    def test_root_pprint(self, root):
        with Capturing() as output:
            root.pprint()
        assert f"base_url: {HOST}" in output
        assert "show_traceback: True" in output

    def test_root_meta(self):
        meta = {"foo": "bar"}
        r = Root(URL(HOST), True, meta=meta)
        assert r.meta == meta


class TestDataclass:
    """Test dataclass formation."""

    def test_listed_pprint(self):
        """Test blacklist and whitelist."""
        kwargs = {"foo": "bar", "_status": 2}
        m = Dataclass(**kwargs)
        with Capturing() as output:
            m.pprint(blacklist=["foo"])
        assert "foo: bar" not in output
        with Capturing() as output:
            m.pprint(whitelist=["foo"])
        assert "foo: bar" in output
        assert "_status: 2" not in output
        with pytest.raises(ValueError):
            m.pprint(whitelist=["foo"], blacklist=["foo"])

    def test_pprint(self):
        kwargs = {"foo": "bar", "_status": 2}
        m = Dataclass(**kwargs)
        with Capturing() as output:
            m.pprint()
        assert "foo: bar" in output
        assert "_status: 2" not in output

    def test_set_private_attr(self):
        m = Dataclass(_foo=1)
        assert m._foo == 1

    def test_set_kwargs_in_response_class(self):
        kwargs = {"foo": "bar", "status": 2}
        m = Dataclass(**kwargs)
        assert m.__dict__ == kwargs

    @dataclass(frozen=True)
    class MockAnyDataclass(Dataclass):
        foo: Any

    def test_any_dataclass(self):
        self.MockAnyDataclass("foo")
        self.MockAnyDataclass(1)
        self.MockAnyDataclass({"foo": "bar"})

    @dataclass(frozen=True)
    class MockDataClass(Dataclass):
        foo: str
        bar: int

    def test_bad_subclass(self):
        with pytest.raises(TypeError):
            self.MockDataClass(2, "foo")

    def test_good_subclass(self):
        self.MockDataClass("foo", 2)

    @pytest.mark.skip("This test isn't working, and I don't have the time to fix it for now")
    def test_no_frozen_subclass(self):
        @dataclass
        class MockNoFrozenClass(Dataclass):
            pass

        with pytest.raises(TypeError):
            MockNoFrozenClass()


class TestBaseClient:
    """Test basic api calls (CRUD)."""

    ENDPOINT = "foo"

    def test_format_resp(self):
        data = {"data": None}
        assert Client._format_resp(data.copy()) != data
        data = {"data": []}
        assert Client._format_resp(data.copy()) != data

    async def test_make_context_switch(self):
        async with Client("foo") as c:
            c

    async def test_make_url(self):
        base = URL("base")
        made = Client(base)._make_url(self.ENDPOINT)
        assert made == base / self.ENDPOINT

    async def test_post(self, aioresponse_mock, root):
        async with root.client() as client:
            aioresponse_mock.post(f"http://test.com/{self.ENDPOINT}", status=200, payload={"data": {}})
            ret = await client.post(endpoint=self.ENDPOINT)
            assert isinstance(ret, Dataclass)
        async with root.client(dataclass=Dataclass) as client:
            aioresponse_mock.post(f"http://test.com/{self.ENDPOINT}", status=200, payload={"data": {}})
            ret = await client.post(endpoint=self.ENDPOINT)
            assert isinstance(ret, Dataclass)

    async def test_get(self, aioresponse_mock, root):
        async with root.client() as client:
            aioresponse_mock.get(f"http://test.com/{self.ENDPOINT}", status=200, payload={"data": {}})
            ret = await client.get(endpoint=self.ENDPOINT)
            assert isinstance(ret, Dataclass)
        async with root.client(dataclass=Dataclass) as client:
            aioresponse_mock.get(f"http://test.com/{self.ENDPOINT}", status=200, payload={"data": {}})
            ret = await client.get(endpoint=self.ENDPOINT)
            assert isinstance(ret, Dataclass)

    async def test_delete(self, aioresponse_mock, root):
        async with root.client() as client:
            aioresponse_mock.delete(f"http://test.com/{self.ENDPOINT}", status=200)
            ret = await client.delete(endpoint=self.ENDPOINT)
            assert ret is None
        async with root.client(dataclass=Dataclass) as client:
            aioresponse_mock.delete(f"http://test.com/{self.ENDPOINT}", status=200)
            ret = await client.delete(endpoint=self.ENDPOINT)
            assert ret is None

    async def test_patch(self, aioresponse_mock, root):
        async with root.client() as client:
            aioresponse_mock.patch(f"http://test.com/{self.ENDPOINT}", status=200, payload={"data": {}})
            ret = await client.patch(endpoint=self.ENDPOINT)
            assert isinstance(ret, Dataclass)
        async with root.client(dataclass=Dataclass) as client:
            aioresponse_mock.patch(f"http://test.com/{self.ENDPOINT}", status=200, payload={"data": {}})
            ret = await client.patch(endpoint=self.ENDPOINT)
            assert isinstance(ret, Dataclass)

    async def test_put(self, aioresponse_mock, root):
        async with root.client() as client:
            aioresponse_mock.put(f"http://test.com/{self.ENDPOINT}", status=200, payload={"data": {}})
            ret = await client.put(endpoint=self.ENDPOINT)
            assert isinstance(ret, Dataclass)
        async with root.client(dataclass=Dataclass) as client:
            aioresponse_mock.put(f"http://test.com/{self.ENDPOINT}", status=200, payload={"data": {}})
            ret = await client.put(endpoint=self.ENDPOINT)
            assert isinstance(ret, Dataclass)
