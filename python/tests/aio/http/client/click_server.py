# type: ignore
# flake8: noqa
import asyncio
from typing import Any, AsyncIterator, Awaitable, Callable, Dict

from aiohttp import web

router = web.RouteTableDef()


def handle_json_error(
    func: Callable[[web.Request], Awaitable[web.Response]]
) -> Callable[[web.Request], Awaitable[web.Response]]:
    async def handler(request: web.Request) -> web.Response:
        try:
            return await func(request)
        except asyncio.CancelledError:
            raise
        except Exception as exc:
            return web.json_response({"status": "failed", "reason": str(exc)}, status=400)

    return handler


@router.get("/")
async def root(request: web.Request) -> web.Response:
    return web.Response(text=f"Placeholder")


@router.get("/api")
async def api_list_posts(request: web.Request) -> web.Response:
    return web.json_response({"status": "ok", "data": []})


@router.get("/api/{post}")
@handle_json_error
async def api_get_post(request: web.Request) -> web.Response:
    post_id = request.match_info["post"]
    return web.json_response(
        {
            "status": "ok",
            "data": {
                "id": int(post_id),
                "owner": "foo",
                "editor": "bar",
                "title": "zot",
            },
        }
    )


async def init_app() -> web.Application:
    app = web.Application()
    app.add_routes(router)
    return app


web.run_app(init_app())
