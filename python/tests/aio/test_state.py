"""Test State."""
import asyncio
from threading import Timer

import pytest

from common.aio.state import StateManager


@pytest.fixture
def clear_state():
    StateManager.clearall()
    yield
    StateManager.clearall()


class TestState:
    SMALL = 0.000001

    def test_started_event(self, clear_state):
        StateManager.start()
        assert StateManager.is_started()
        StateManager.stop()
        assert StateManager.is_stopped()

    def test_processing_event(self, clear_state):
        StateManager.start_processing()
        assert StateManager.is_processing()
        StateManager.stop_processing()
        assert not StateManager.is_processing()

    def test_fast_forward_event(self, clear_state):
        StateManager.fast()
        assert StateManager.is_fast_forwarding()
        StateManager.slow()
        assert not StateManager.is_fast_forwarding()

    def test_paused_event(self, clear_state):
        StateManager.pause()
        assert StateManager.is_paused()
        StateManager.unpause()
        assert not StateManager.is_paused()

    def test_cancel_event(self, clear_state):
        StateManager.cancel()
        assert StateManager.is_cancelled()

    def test_shutdown_event(self, clear_state):
        StateManager.shutdown()
        assert StateManager.is_shutdown()

    async def test_wait_on_set(self, mocker, clear_state):
        spy_sleep = mocker.spy(asyncio, "sleep")
        t = Timer(StateManager.INTERVAL / 2, StateManager.start)
        t.start()
        await StateManager.wait_on_set(StateManager.STARTED)
        spy_sleep.assert_called_once()

    async def test_wait_on_set_unless(self, mocker, clear_state):
        spy_sleep = mocker.spy(asyncio, "sleep")
        t = Timer(StateManager.INTERVAL / 2, StateManager.start)
        t.start()
        StateManager.shutdown()
        await StateManager.wait_on_set(StateManager.STARTED, unless=StateManager.SHUTDOWN)
        spy_sleep.assert_not_called()

    async def test_wait_on_unset(self, mocker, clear_state):
        spy_sleep = mocker.spy(asyncio, "sleep")
        StateManager.start()
        t = Timer(StateManager.INTERVAL + self.SMALL, StateManager.stop)
        t.start()
        await StateManager.wait_on_unset(StateManager.STARTED)
        spy_sleep.assert_called()

    async def test_wait_on_unset_unless(self, mocker, clear_state):
        spy_sleep = mocker.spy(asyncio, "sleep")
        StateManager.start()
        t = Timer(StateManager.INTERVAL + self.SMALL, StateManager.stop)
        t.start()
        StateManager.shutdown()
        await StateManager.wait_on_unset(StateManager.STARTED, unless=StateManager.SHUTDOWN)
        spy_sleep.assert_not_called()

    async def test_custom_sleep(self, mocker, clear_state):
        spy_sleep = mocker.spy(asyncio, "sleep")
        await StateManager.sleep(0)
        spy_sleep.assert_called_once()

        spy_sleep.reset_mock()
        t = Timer(self.SMALL, StateManager.cancel)
        t.start()
        await StateManager.sleep(StateManager.INTERVAL * 3)
        spy_sleep.assert_called_once()

        spy_sleep.reset_mock()
        spy_sleep.side_effect = RuntimeError()
        await StateManager.sleep(0)
        spy_sleep.assert_called_once()

    async def test_catch_cancel(self, clear_state):
        async def inner():
            StateManager.check_cancel()

        StateManager.cancel()
        assert not await StateManager.catch_cancel(StateManager.check_cancel)
        StateManager.clearall()

        StateManager.cancel()
        assert not await StateManager.catch_cancel(inner)
        StateManager.clearall()

        StateManager.cancel()
        assert await StateManager.catch_cancel(None)

    async def test_catch_shutdown(self, clear_state):
        async def inner():
            StateManager.check_shutdown()

        StateManager.shutdown()
        assert not await StateManager.catch_shutdown(StateManager.check_shutdown)
        StateManager.clearall()

        StateManager.shutdown()
        assert not await StateManager.catch_shutdown(inner)
        StateManager.clearall()

        StateManager.shutdown()
        assert await StateManager.catch_shutdown(None)
