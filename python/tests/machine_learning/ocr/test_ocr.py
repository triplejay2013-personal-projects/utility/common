import logging
from pathlib import Path

import pytest
from PIL import Image

from common.machine_learning.image.processing import Color, OcrImageProcessor
from common.machine_learning.ocr import OCR, PSM

IMAGES = Path(__file__).absolute().parent.parent / "images"

logger = logging.getLogger(__name__)


class TestOCR:
    """Test OCR img_to_str/int.

    My initial attempts at OCR in python was in relation to a btd6 bot I created. The images
    used in these tests are pulled from there.
    """

    def test_obj_creation(self):
        OCR(PSM.SINGLE_LINE)
        OCR("SINGLE_LINE")
        with pytest.raises(ValueError):
            OCR("SINGLE_")

    def test_image_to_str(self):
        img = OcrImageProcessor.invert(Image.open(str(IMAGES / "victory.png")), Color.YELLOW)
        assert OCR(PSM.SINGLE_LINE).image_to_str(img) == "victory"
        img = OcrImageProcessor.invert(Image.open(str(IMAGES / "level_up.png")), Color.WHITE)
        assert OCR(PSM.SINGLE_LINE).image_to_str(img) == "levelup"
        img = OcrImageProcessor.invert(Image.open(str(IMAGES / "level_up.png")), Color.WHITE)
        assert OCR(PSM.SINGLE_COLUMN).image_to_str(img) == "levelup"
        img = OcrImageProcessor.invert(Image.open(str(IMAGES / "level_up.png")), Color.WHITE)
        assert OCR(PSM.SINGLE_COLUMN).image_to_str(img) == "levelup"
        # An image with text in two lines
        img = OcrImageProcessor.invert(Image.open(str(IMAGES / "bonus_rewards.png")), Color.LIGHT_YELLOW)
        assert OCR(PSM.SINGLE_BLOCK).image_to_str(img) == "bonusrewards"
        img = OcrImageProcessor.invert(Image.open(str(IMAGES / "round.png")), Color.WHITE)
        assert OCR(PSM.SINGLE_LINE, "/").image_to_str(img) == "6/100"
        img = OcrImageProcessor.invert(Image.open(str(IMAGES / "map_tile.png")), Color.WHITE)
        assert OCR(PSM.SPARSE_TEXT).image_to_str(img) == "muddypuddles"

    def test_image_to_int(self):
        img = OcrImageProcessor.invert(Image.open(str(IMAGES / "health.png")), Color.WHITE)
        assert OCR(PSM.SINGLE_LINE).image_to_int(img) == 1
        img = OcrImageProcessor.invert(Image.open(str(IMAGES / "gold.png")), Color.WHITE)
        assert OCR(PSM.SINGLE_LINE, whitelist="$").image_to_int(img) == 650
