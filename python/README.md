# About

Provide common methods and modules that I can reuse across many projects

# TODO

* auto versioning/packaging on push (git hook)
* pre-commit hooks (currently in asr-app) belong here (probably in parent package or something, not in python/ dir)

## pip install

```
pip install git+https://gitlab.com/triplejay2013-personal-projects/utility/common.git#subdirectory=python&egg=common-<version>
```

OR

Provide a `pypirc`:
```
[gitlab]
repository = https://gitlab.com/api/v4/projects/26999359/packages/pypi
username = __token__
password = <your personal access token>
```
and run
```
pip3 install common --extra-index-url https://__token__:<your_personal_token>@gitlab.com/api/v4/projects/26999359/packages/pypi/simple
```
Use --index-url to avoid lookup conflicts with the public pypi packages

OR

clone this repository, and install directly from built wheels
```
pip3 install <path_to_common_repo>/python/dist/common-<ver>.whl
```
