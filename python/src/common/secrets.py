# type: ignore
# flake8: noqa
"""Manage secrets using python Fernet encryption."""

from pathlib import Path
from typing import Callable

from cryptography.fernet import Fernet


def force_param_types(*args, _type: type = str, frule: Callable = None):
    """Make sure our args are _type.

    Params:
        args: The args we want to force type upon
        _type: The type to force (str, bytes, etc.)
        frule: Describes how to format params (format rule)

    Returns:
        args formatted according to _type
    """
    ret = []
    for a in args:
        if not isinstance(a, _type):
            ret.append(frule(a))
        else:
            ret.append(a)
    return ret


# Use like **rule
BYTE_RULE = {
    "_type": bytes,
    "frule": lambda x: x.encode("utf-8"),
}
STR_RULE = {
    "_type": str,
    "frule": lambda x: x.decode("utf-8"),
}


def load_key_file(filename: str) -> bytes:
    """Loads key from secrets file.

    Params:
        filename (str): The name of the keyfile. Will always assume it is located at /home/<user>

    Returns:
        bytes loaded from the file
    """
    try:
        # Expects keyfile to be in home dir by default
        path = Path(Path.home() / filename)
        with open(path, "rb") as f:
            key = f.read()
    except:
        print(
            f"Check your password manager for a Fernet Key (python), and place it here '{Path.home()}' as '<filename>'"
        )
        return b""  # Don't raise stack trace (messy output)
    return key


# NOTE: in encrypt/decrypt, the weird var with a comma assignment is to unpack
# a single element list into a variable


def encrypt(message: str, key_file: str) -> bytes:
    """Encrypt a message with Python Fernet.

    Params:
        message (str): The message to encrypt
        key_file (str): Key to use in encryption

    Returns:
        bytes of encrypted data
    """
    message, key = force_param_types(message, load_key_file(key_file), **BYTE_RULE)
    (secret,) = force_param_types(Fernet(key).encrypt(message), **BYTE_RULE)
    return secret


def decrypt(secrets: bytes, key_file: str) -> str:
    """Decrypt a message with Python Fernet.

    Params:
        secrets (str): The secrets to decrypt
        key_file (str): Key to use in decryption

    Returns:
        str of decrypted data
    """
    secrets, key = force_param_types(secrets, load_key_file(key_file), **BYTE_RULE)
    (message,) = force_param_types(Fernet(key).decrypt(secrets), **STR_RULE)
    return message


if __name__ == "__main__":
    enc = encrypt("Hello World")
    print(enc)
    print(decrypt(enc))
