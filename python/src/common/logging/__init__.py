import logging
import os
import sys

# from common.aio._lock import KILL_SWITCH
from common.aio.lock import LockManager

# https://docs.python.org/3/library/logging.html#logrecord-attributes
FORMAT = logging.Formatter(
    "%(asctime)s [%(levelname)-8s] [%(name)-23s] [%(threadName)-23s] [%(filename)s:%(lineno)s]\t%(message)s"
)

r_factory_orig = logging.getLogRecordFactory()


class LoggerWrap(logging.Logger):
    """Wrap logger class to use with io locking that is in place."""

    # https://github.com/python/cpython/blob/3.10/Lib/logging/__init__.py#L1600
    def _log(self, *args, **kwargs):
        # This method is called by all other logger methods (info/debug/critical etc)
        with LockManager.io_lock(self):
            # Don't hold onto an io_lock for too long
            # TODO - KILL_SWITCH launches too many threads. This needs to be reworked
            # KILL_SWITCH.start()
            # TODO - this needs tested with multiple threads still. Sometimes threads without an rlock will
            #  acquire the lock somehow, and output when a block lock is in place. This isn't ideal
            super()._log(*args, **kwargs)
            # KILL_SWITCH.cancel()


def record_factory(*args, **kwargs) -> logging.LogRecord:
    """Custom log handlers and attributes."""
    record = r_factory_orig(*args, **kwargs)
    # Add attributes
    # example:
    #  record.foo = record.bar.replace("foo", "bar")
    return record


# TODO - allow cmdline args
def setup_logging():
    truthy = ["true", "1"]

    logging.setLoggerClass(LoggerWrap)
    # Add a new log level called TRACE. This is more verbose than DEBUG
    logging.TRACE = 5
    logging.addLevelName(logging.TRACE, "TRACE")
    logging.Logger.trace = lambda inst, msg, *args, **kwargs: inst.log(logging.TRACE, msg, *args, **kwargs)

    level = logging.INFO
    if os.environ.get("DEBUG", False) in truthy:
        level = logging.DEBUG
    if os.environ.get("TRACE", False) in truthy:
        level = logging.TRACE
    logging.basicConfig(level=level)
    logging.setLogRecordFactory(record_factory)
    root = logging.getLogger()
    # Root logger must be set to lowest possible level, and individual handlers
    # can then parse out logs they don't need
    root.setLevel(logging.TRACE)
    root.handlers = []

    stdout_h = logging.StreamHandler(sys.stdout)
    stdout_h.setFormatter(FORMAT)
    stdout_h.setLevel(level)  # TODO customize with cmdline args
    root.addHandler(stdout_h)

    file_dir = os.getenv("COMMON_LOGGING_DIR", ".")
    file_h = logging.FileHandler(f"{file_dir}/log.txt", mode="w+")
    file_h.setFormatter(FORMAT)
    file_h.setLevel(logging.TRACE)
    root.addHandler(file_h)

    # Override loggers
    # example:
    #  logging.getLogger(<NAME>).setLevel(logging.ERROR)
    logging.getLogger("asyncio").setLevel(logging.ERROR)
