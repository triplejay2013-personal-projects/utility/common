"""Parse complex dictionary structures."""
from typing import Dict, List, Optional


class ParseDict(dict):
    """Provides utility to parse complex dictionary constructs."""

    def _match(self, container: Dict, search: str, path: str) -> List[str]:
        """Define helper function for `match()`.

        Args:
            container (Dict): dictionary structure to search
            search (List): List of strings to match against (case-sensitively)
            path (str): the current path to build
        Returns:
            All key-paths that correspond to the search
        """
        found = []
        for key, val in container.items():
            if search == key or search == str(val):
                found.append(path)
            new_path = f"{path}.{key}" if path else key
            # Just keep walking
            if isinstance(val, list):
                index = 0
                for elem in val:
                    if isinstance(elem, str):
                        if search == str(elem):
                            found.extend(path)
                    else:
                        ret = self._match(elem, search, f"{new_path}.{index}")
                        if ret:
                            found.extend(ret)
                    index += 1
            elif isinstance(val, dict):
                ret = self._match(val, search, new_path)
                if ret:
                    found.extend(ret)
        return found

    def match(self, search: List[str]) -> List[str]:
        """Walk the dictionary in search of 'search'.

        It accumulates all paths that have that match.

        Args:
            search (List): List of strings to match against (case-sensitively)
        Returns:
            All key-paths that correspond to the search
        """
        paths = []
        for item in search:
            paths.extend(self._match(self, item, ""))
        return list(set(paths))

    def get(self, path: str, default=None):
        """Retrieve a value from a dictionary by it's path.

        Args:
            path (str): path made up of keys separated by '.'
            default: Default value to return if value does not exist
        """
        keys = path.split(".")
        val: Optional[Dict] = None

        for key in keys:
            if key.isdigit() and isinstance(val, list):
                val = val[int(key)]
                continue  # Move on to the next key
            if val:
                if isinstance(val, list):
                    val = [v.get(key, default) if v else None for v in val]
                else:
                    val = val.get(key, default)
            else:
                val = dict.get(self, key, default)

            if not val:
                break
        if val is None:  # Edge case, dict.get sometimes doesn't return default
            return default
        return val
