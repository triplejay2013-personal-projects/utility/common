"""Instantiate locks in common location to avoid circular deps."""
from threading import Lock, RLock, Timer


class KillSwitchException(Exception):
    pass


def kill_switch():
    raise KillSwitchException()


# Raise Exception to avoid lock deadlocks.
# ie, if a lock is held longer than 30s, we are stuck
KILL_SWITCH = Timer(30.0, kill_switch)


# Reentrant lock for IO operations (same thread doesn't block)
IO_LOCK = RLock()

# Normal Lock. Blocks on each request
SYSTEM_LOCK = Lock()
