"""Manage event state of an application."""
import asyncio
import logging
from typing import Callable, Tuple

import keyboard
import mouse

logger = logging.getLogger(__name__)


class Cancel(Exception):
    """Indicates a run has been cancelled."""


class Shutdown(Exception):
    """Indicates we are shutting down."""


class StateManager:
    """Controls signal events.

    Attributes:
        STARTED/STOPPED: Indicates we have began a run, if unset, we are STOPPED
        PROCESSING: Currently processing an event
        FAST_FORWARD: Are we fast forwarding right now.
        PAUSED: Stop events (TODO unimplemented/unused)
        CANCEL: We are cancelling a certain run
        SHUTDOWN: Indicates we are killing the bot
        INTERVAL: How often we will check for flag updates in-between sleeps
    """

    STARTED = asyncio.Event()
    PROCESSING = asyncio.Event()
    FAST_FORWARD = asyncio.Event()
    PAUSED = asyncio.Event()
    CANCEL = asyncio.Event()
    SHUTDOWN = asyncio.Event()

    INTERVAL = 0.2

    @classmethod
    def start_processing(cls):
        logger.trace("PROCESSING set")  # type: ignore[attr-defined]
        cls.PROCESSING.set()

    @classmethod
    def stop_processing(cls):
        logger.trace("PROCESSING set")  # type: ignore[attr-defined]
        cls.PROCESSING.clear()

    @classmethod
    def fast(cls):
        cls.FAST_FORWARD.set()

    @classmethod
    def slow(cls):
        cls.FAST_FORWARD.clear()

    @classmethod
    def is_fast_forwarding(cls) -> bool:
        return cls.FAST_FORWARD.is_set()

    @classmethod
    def is_processing(cls) -> bool:
        return cls.PROCESSING.is_set()

    @classmethod
    def shutdown(cls):
        logger.trace("SHUTDOWN set")  # type: ignore[attr-defined]
        cls.SHUTDOWN.set()

    @classmethod
    def is_shutdown(cls):
        return cls.SHUTDOWN.is_set()

    @classmethod
    def pause(cls):
        logger.trace("PAUSED set")  # type: ignore[attr-defined]
        cls.PAUSED.set()

    @classmethod
    def is_paused(cls):
        return cls.PAUSED.is_set()

    @classmethod
    def unpause(cls):
        logger.trace("PAUSED unset")  # type: ignore[attr-defined]
        cls.PAUSED.clear()

    @classmethod
    def cancel(cls):
        logger.trace("CANCEL set")  # type: ignore[attr-defined]
        cls.CANCEL.set()

    @classmethod
    def is_cancelled(cls):
        return cls.CANCEL.is_set()

    @classmethod
    def start(cls):
        logger.trace("START set")  # type: ignore[attr-defined]
        cls.STARTED.set()

    @classmethod
    def is_started(cls):
        return cls.STARTED.is_set()

    @classmethod
    def stop(cls):
        logger.trace("START unset")  # type: ignore[attr-defined]
        cls.STARTED.clear()

    @classmethod
    def is_stopped(cls):
        return not cls.STARTED.is_set()

    @classmethod
    async def wait_on_set(cls, flag: asyncio.Event, unless: asyncio.Event = None):
        """Wait until a flag is set unless another optional flag becomes set."""
        while not flag.is_set():
            if unless and unless.is_set():
                break
            await asyncio.sleep(cls.INTERVAL)

    @classmethod
    async def wait_on_unset(cls, flag: asyncio.Event, unless: asyncio.Event = None):
        """Wait until a flag is unset unless another optional flag becomes set."""
        while flag.is_set():
            if unless and unless.is_set():
                break
            await asyncio.sleep(cls.INTERVAL)

    @classmethod
    def clearall(cls):
        cls.STARTED.clear()
        cls.CANCEL.clear()
        cls.PAUSED.clear()
        cls.SHUTDOWN.clear()
        cls.PROCESSING.clear()
        cls.FAST_FORWARD.clear()

    @classmethod
    async def sleep(cls, t: float):
        """Custom sleep to allow more responsive CANCEL flag (doesn't get stuck waiting for async sleep)."""
        while t >= 0:
            try:
                await asyncio.sleep(cls.INTERVAL)
            except RuntimeError:
                # If there is no event loop, just exit
                return
            if cls.CANCEL.is_set():
                break
            t -= cls.INTERVAL

    @classmethod
    async def watch_kill_switch(cls):
        """This is where we capture kill events."""
        while not cls.is_shutdown():  # Run until killed
            while not cls.is_cancelled():
                if keyboard.is_pressed("ctrl+c") or mouse.get_position() == (0, 0):
                    cls.cancel()
                    print("Cancelling run")
                if cls.is_shutdown():
                    break
                await cls.sleep(0)  # Give up event loop for waiting tasks
            await cls.sleep(0)

    @classmethod
    async def watch_shutdown_capture(cls, screen_resolution: Tuple[int, int]):
        """Capture shutdown event."""
        DELTA = 2  # If we are within 2 pixels, it's good enough
        while not cls.is_shutdown():  # Run until killed (by this or natural execution)
            while not cls.is_cancelled():
                pos = mouse.get_position()
                x = abs(pos[0] - screen_resolution[0])
                if x < DELTA and pos[1] == 0:
                    logger.info("Captured shutdown event")
                    cls.cancel()
                    cls.shutdown()
                if cls.is_shutdown():
                    break
                await cls.sleep(0)
            await cls.sleep(0)

    @classmethod
    def check_shutdown(cls):
        """Useful to shutdown async threadpools."""
        if cls.is_shutdown():
            raise Shutdown()

    @classmethod
    def check_cancel(cls):
        """Useful in loops/conditional checks.

        Notes:
            Checks Cancel flag and raises exception when found
        """
        if cls.is_cancelled():
            raise Cancel()

    @staticmethod
    async def catch_cancel(fn: Callable) -> bool:
        """Try to run function in context of StateManager."""
        try:
            if asyncio.iscoroutinefunction(fn):
                await fn()
            elif callable(fn):
                fn()
            else:
                pass  # NoOp
        except Cancel:
            return False
        return True

    @staticmethod
    async def catch_shutdown(fn: Callable) -> bool:
        """Try to run function in context of StateManager."""
        try:
            if asyncio.iscoroutinefunction(fn):
                await fn()
            elif callable(fn):
                fn()
            else:
                pass  # NoOp
        except Shutdown:
            return False
        return True
