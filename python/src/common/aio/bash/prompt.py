# type: ignore
# flake8: noqa
"""Bash Prompt with python asyncio."""
import argparse
import asyncio
import re
from pathlib import Path


def decode(x, y):
    return (x.decode("utf-8").strip("\n"), y.decode("utf-8").strip("\n"))


class NonZeroExitCode(Exception):
    """NonZero exit code occurred"""


async def run(command: str, quiet: bool = False) -> str:
    """Run commands (async) in shell.

    Parameters:
        command (str): The command to execute in the shell
        quiet (bool): Hide ALL output (stderr AND stdout)

    Returns:
        Tuple stdout, stderr

    Notes:
        This cannot handle complex tasks
    """
    proc = await asyncio.create_subprocess_shell(
        command, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE, cwd=Path.cwd()
    )
    stdout, stderr = await proc.communicate()
    stdout, stderr = decode(stdout, stderr)
    if stderr and not quiet:
        print(stderr)
    if proc.returncode != 0:
        raise NonZeroExitCode()
    return stdout


class Bash:
    """Bash operations."""

    async def machine_name(self):
        """Get machine name."""
        name = await run("hostname")
        return name


class Git:
    """Git read-only operations."""

    def __init__(self, branch: str):
        # git branch shows branches, and an '*' for the active one
        try:
            mystr = [s for s in branch.split("\n") if "*" in s and "detached" not in s][0]
            self.branch = re.sub(r"\*\s(.*)", r"\1", mystr)
        except IndexError:
            # No matches found
            self.branch = None

    async def root(self):
        """Get git root."""
        try:
            resp = await run("git root")
        except NonZeroExitCode:
            pass
        return resp

    async def branch_needs_update(self) -> bool:
        """Check to see if branch needs to be updated."""
        if self.branch:
            # Quick way to check we are out of date. If there is no diff, we are up-to-date
            # NOTE: This quick trick may not work for some remotes
            # NOTE: This is MUCH faster than `fetch --dry-run -v 2>&1`
            try:
                resp = await run(f"git diff origin/{self.branch}", quiet=True)
            except NonZeroExitCode:
                return True  # origin may not exist, probably needs an update
            else:
                # TODO: BUG this doesn't work. It shows txt even when up-to-date with remote code. Find a way to indicate
                # that we are up-to-date with remote, and don't show text if only local code is different
                return not not resp  # Converts to bool
        return True

    async def branch_count(self, is_local: bool = True) -> int:
        """Count number of remote/local branches."""
        cmd = "git branch" if is_local else "git branch -r"
        cmd += " | wc -l"
        try:
            resp = await run(cmd)
        except NonZeroExitCode:
            return 0
        return int(resp)

    async def commit_count(self) -> int:
        try:
            resp = await run("git rev-list HEAD --count", quiet=True)
            return int(resp)
        except (NonZeroExitCode, ValueError):
            return 0


async def bash_prompt():
    """Collect git info and print.

    Notes:
        It is expected to capture output in a bash script if using this command
    """
    try:
        # Only run if we are in a git dir
        branch = await run("git branch", quiet=True)
    except NonZeroExitCode:
        return 1
    else:
        try:
            g = Git(branch)
            b = Bash()
        except Exception as e:
            print(f'echo "Ran into error in mybashprompt: {e}"')
        else:
            print('__GIT_BRANCH="%s"' % (g.branch if g.branch else "detached"))
            print('__GIT_BRANCH_NEEDS_UPDATE="%s"' % await g.branch_needs_update())
            print('__GIT_LOCAL_BRANCH_COUNT="%s"' % await g.branch_count())
            print('__GIT_COMMIT_COUNT="%s"' % await g.commit_count())
            print('__GIT_REMOTE_BRANCH_COUNT="%s"' % await g.branch_count(False))
            print('__GIT_ROOT="%s"' % await g.root())
            print('__MACHINE_NAME="%s"' % await b.machine_name())


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("command", type=str)
    command = parser.parse_args().command
    asyncio.run(bash_prompt())


if __name__ == "__main__":
    main()
