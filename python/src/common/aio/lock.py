"""Manage event locks."""
import logging
import time
from contextlib import asynccontextmanager, contextmanager
from threading import Lock
from typing import Any, AsyncIterator, Iterator, List, Tuple, Union

import keyboard
import mouse

from common.aio._lock import IO_LOCK, SYSTEM_LOCK
from common.aio.config import SYSTEM_OP_DELAY
from common.aio.state import Cancel, Shutdown, StateManager

logger = logging.getLogger(__name__)


# TODO - add failsafe timeout. After (ie 10s), the lock will be reacquired
# Forcefully, and an exception raised. This avoids locking up the system
# This should be logged, and fixed. Ideally this type of system lockup shouldn't
# occur, but recording this in a log, can help identify edge cases in the future.


class LockManager:
    """Manage Operation locks.

    Attributes:
        IO_LOCK: Helps control printing of messages
        SYSTEM_LOCK: Helps control mouse/keyboard operations
    """

    @classmethod
    async def _lock(cls, lock: Lock):
        while lock.locked():
            if StateManager.is_cancelled() and lock == SYSTEM_LOCK:
                # Don't let things capture locks if we have cancelled the run
                raise Cancel()
            if StateManager.is_shutdown() and lock == SYSTEM_LOCK:
                # Kill requests to the lock on shutdown
                raise Shutdown()
            await StateManager.sleep(0)

    @classmethod
    def _validate(cls, actions: Union[List[str], str]):
        if isinstance(actions, List):
            return actions
        return [actions]

    @classmethod
    @contextmanager
    def system_lock(cls, actions: Union[List[str], str]) -> Iterator[Tuple[Any, ...]]:
        """Avoid locking large segments of code.

        Only lock what you need to execute without interference.
        """
        with SYSTEM_LOCK:
            logger.trace("SYSTEM_LOCK_ACQUIRED")  # type: ignore[attr-defined]
            action_l = tuple(getattr(cls, f"_{cls.__name__}__{a}") for a in cls._validate(actions))
            logger.trace("Yielding system-action-lock: %s", actions)  # type: ignore[attr-defined]
            yield action_l
        logger.trace("SYSTEM_LOCK_RELEASED")  # type: ignore[attr-defined]

    @classmethod
    @contextmanager
    def io_lock(cls, actions: Union[List[str], str, logging.Logger]) -> Iterator[Tuple[Any, ...]]:
        """Avoid locking large segments of code.

        Only lock what you need to execute without interference.
        Input operations set a blocking lock, output operations do not.
        This is to help avoid outputting text when the cli is asking for input
        """
        if isinstance(actions, List) or isinstance(actions, str):
            input_operation = "input" in actions or actions == "input"
        else:
            input_operation = False
        IO_LOCK.acquire(input_operation)
        # logger.trace("IO_LOCK_ACQUIRED")  # type: ignore[attr-defined]
        try:
            # We don't use context manager, so we can specify the type of lock (blocking or not)
            if isinstance(actions, logging.Logger):
                yield tuple()
            else:
                action_l = tuple(getattr(cls, f"_{cls.__name__}__{a}") for a in cls._validate(actions))
                yield action_l
        except Exception as e:
            logger.exception("Ran into error while lock was enabled. Releasing Lock: %s", e)
        finally:
            IO_LOCK.release()
        # logger.trace("IO_LOCK_RELEASED")  # type: ignore[attr-defined]

    @classmethod
    @asynccontextmanager
    async def async_system_lock(cls, actions: Union[List[str], str]) -> AsyncIterator[Tuple]:
        """Avoid locking large segments of code.

        Only lock what you need to execute without interference.
        """
        logger.trace("Waiting for SYSTEM_LOCK. Requesting: %s", actions)  # type: ignore[attr-defined]
        await cls._lock(SYSTEM_LOCK)
        with cls.system_lock(actions) as res:
            yield res

    @classmethod
    @asynccontextmanager
    async def async_io_lock(cls, actions: Union[List[str], str, logging.Logger]) -> AsyncIterator[Tuple]:
        """Avoid locking large segments of code.

        Only lock what you need to execute without interference.
        """
        with cls.io_lock(actions) as res:
            yield res

    @classmethod
    def __input(cls, *args, **kwargs):
        """Helps manage io."""
        return input(*args, **kwargs)

    @classmethod
    def __print(cls, *args, **kwargs):
        """Help manage messages from multiple threads."""
        print(*args, **kwargs)

    @classmethod
    def __move_and_click(cls, coordinates, *args, **kwargs):
        """Move and Click mouse with SystemLock.

        Args:
            coordinates (XY): The place to move before click
        """
        mouse.move(*coordinates, *args, **kwargs)
        time.sleep(SYSTEM_OP_DELAY)
        mouse.click(mouse.LEFT)

    @classmethod
    def __press_and_release(cls, *args, **kwargs):
        """Type with the keyboard."""
        keyboard.press_and_release(*args, **kwargs)
