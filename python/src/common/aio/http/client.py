# type: ignore
# flake8: noqa
import asyncio  # async calls
import functools  # Wrap func for click calls
import re  # Regex matching
from contextlib import asynccontextmanager  # Form context-capable methods
from dataclasses import (  # Succinct classes (Read-only in our case)
    dataclass,
    field,
    is_dataclass,
)
from types import TracebackType  # Provides more use-specific 'builtin' types

# Type hinting
from typing import (
    Any,
    AsyncIterator,
    Dict,
    List,
    Mapping,
    Optional,
    Type,
    Union,
    get_args,
    get_origin,
)

import aiohttp  # Async http calls
import click  # Provide cmd-line interface for our client calls
from yarl import URL  # Immutable URL with accessible properties

# See test suite for examples


class Simpleclass:
    """A dataclass (without actually using dataclass).

    Notes:
        Implements same thing as Dataclass, but doesn't use dataclass.
    """

    def __init__(self, *args, **kwargs):
        """Initialize properties with whatever is given in args/kwargs."""
        for k, v in kwargs.items():
            self.__setattr__(k, v)
        self.args = args

    def pprint(self, *, blacklist: List = [], whitelist: List = [], msg: str = None) -> None:
        """Pretty print our response to click.

        Args:
            *: Only allow kwargs
            blacklist (List): Don't print anything in this list
            whitelist (List): Only print what is in this list
            msg (str): Print this message as well (first)
        """

        def print_list(val: List):
            for l in val:
                click.echo(f"\t{l}")

        if msg:
            click.echo(msg)

        for k, v in self.__dict__.items():
            if k in blacklist and k in whitelist:
                raise ValueError(f"Found {k} in whitelist and blacklist")
            if whitelist:
                if k in whitelist:
                    if isinstance(v, list):
                        print_list(v)
                    else:
                        click.echo(f"{k}: {v}")
            elif not k.startswith("_") and not k.endswith("_") and k not in blacklist:
                if isinstance(v, list):
                    print_list(v)
                else:
                    click.echo(f"{k}: {v}")


@dataclass(frozen=True)
class Dataclass:
    """Base dataclass.

    Note:
        ALL subclasses MUST be frozen (makes the class pseudo-immutable)
        Will accept dynamic attributes on init
        Performs type validation
    """

    def __new__(cls, *args, **kwargs):
        """Provide unkown kwargs to dataclass.

        Note:
            https://stackoverflow.com/a/63291704

            If subclassed, the `kwarg` functionality will not work. By subclassing
            you are specifying 'specific' fields you want to look for. If you want
            this dynamic behaviour, do not specify any __annotations__

            or you can specify your own kwargs/meta argument. This also has ramifications
            (ie cannot easily subclass due to MRO)

            example:
                meta: field(default_factory=dict) = None

            BUG (TODO): If subclassed, and kwargs are supplied, an object cannot be
            instantiated via `initializer`. TMPFIX - don't supply kwargs :)
        """
        try:
            initializer = cls.__initializer
        except AttributeError:
            # Store the original init on the class in a different place
            cls.__initializer = initializer = cls.__init__
            # replace init with something harmless
            cls.__init__ = lambda *a, **k: None

        added_args = {}
        for name in list(kwargs.keys()):
            if not hasattr(cls, "__annotations__") or name not in cls.__annotations__:
                added_args[name] = kwargs.pop(name)

        obj = object.__new__(cls)
        if hasattr(cls, "__annotations__") and args:
            # supply args via kwargs (overwrite existing kwargs)
            added_args.update(dict(zip(cls.__annotations__.keys(), args)))
        try:
            initializer(obj, **kwargs)
        except TypeError as e:
            regex = re.compile(r"'(\w+)*'")  # Matches 'quoted' words
            missing = regex.findall(next(iter(e.args)))
            kwargs.update(dict(zip(missing, args)))
            initializer(obj, **kwargs)
        finally:
            # Add the new args by hand
            for new_name, new_val in added_args.items():
                if not hasattr(obj, new_name):
                    # Work around for frozen dataclass
                    object.__setattr__(obj, new_name, new_val)
        return obj

    def __post_init__(self):
        """Validate annotation types and prep args."""
        try:
            if not is_dataclass(self) or not self.__dataclass_params__.frozen:
                raise TypeError("Must decorate class with @dataclass(frozen=True)")
            # Check for kwargs if subclass defines it
            if hasattr(self, "meta") and isinstance(self.meta, dict):
                # Setup kwargs (we do the object.__setattr__ to avoid FrozenInstanceError)
                [object.__setattr__(self, k, v) for k, v in self.meta.items()]

            if not hasattr(self, "__annotations__"):
                # Occurs with empty dataclass
                return

            for name, field_type in self.__annotations__.items():
                if self.__dict__.get(name) is None:
                    # Ignore annotations we haven't set yet (end of __new__)
                    continue
                # Ignore private attributes and kwargs
                if not name.startswith("_") and name != "meta":
                    if field_type is Any:
                        # Allow any types to be....anything
                        failed_validation = False
                    # Check if is Optional (see: https://stackoverflow.com/a/58841311)
                    elif get_origin(field_type) is Union and type(None) in get_args(field_type):
                        for f in get_args(field_type):
                            if isinstance(self.__dict__[name], f):
                                failed_validation = False
                                break
                    elif isinstance(self.__dict__[name], field_type):
                        failed_validation = False
                    else:
                        failed_validation = True

                    if failed_validation:
                        current_type = type(self.__dict__[name])
                        raise TypeError(f"The field '{name}' was assigned by '{current_type}' instead of {field_type}")
        except Exception as e:
            e.args += ("Dataclass.__post_init__ failed validation",)
            raise e

    def pprint(self, *, blacklist: List = [], whitelist: List = [], msg: str = None) -> None:
        """Pretty print our response to click.

        Args:
            *: Only allow kwargs
            blacklist (List): Don't print anything in this list
            whitelist (List): Only print what is in this list
            msg (str): Print this message as well (first)
        """

        def print_list(val: List):
            for l in val:
                click.echo(f"\t{l}")

        if msg:
            click.echo(msg)

        for k, v in self.__dict__.items():
            if k in blacklist and k in whitelist:
                raise ValueError(f"Found {k} in whitelist and blacklist")
            if whitelist:
                if k in whitelist:
                    if isinstance(v, list):
                        print_list(v)
                    else:
                        click.echo(f"{k}: {v}")
            elif not k.startswith("_") and not k.endswith("_") and k not in blacklist:
                if isinstance(v, list):
                    print_list(v)
                else:
                    click.echo(f"{k}: {v}")


class Client:
    """Async web client."""

    def __init__(self, base_url: URL, dataclass=Dataclass) -> None:
        """Constructor.

        Args:
            base_url (URL): base host we will use to make other calls
            dataclass (Dataclass): class with @dataclass decorator to use when formatting responses.

        Attributes:
            _base_url (URL): see `base_url`
            _client (aiohttp.ClientSession): async session for all get/post/update/delete calls
            _dataclass (Dataclass): see `dataclass`
        """
        self._dataclass = dataclass
        self._base_url = base_url
        self._client = aiohttp.ClientSession(raise_for_status=True, timeout=aiohttp.ClientTimeout(total=15))

    async def close(self) -> None:
        """Close our session connection."""
        return await self._client.close()

    async def __aenter__(self) -> "Client":
        """Async context entrypoint."""
        return self

    async def __aexit__(
        self,
        exc_type: Optional[Type[BaseException]],
        exc_val: Optional[BaseException],
        exc_tb: Optional[TracebackType],
    ) -> Optional[bool]:
        """Async context cleanup."""
        await self.close()

    def _make_url(self, path: str) -> URL:
        """Generate paths off of our base url."""
        return self._base_url / path

    @staticmethod
    def _format_resp(resp: Dict):
        """Format a response to be somewhat what we expect."""
        if resp["data"] is None:
            resp["data"] = {}  # Avoid NoneType
        if not isinstance(resp["data"], Mapping):
            # Make sure we are a mapping for **O operator
            resp["data"] = {"content": resp["data"]}
        return resp

    async def post(self, endpoint: str, include_status: bool = False, *args, **kwargs) -> Dataclass:
        """POST to an endpoint.

        Args:
            endpoint (str): The path to append to base url
            include_status (bool): Add status code to returned class

        Return:
            Dataclass from our request
        """
        async with self._client.post(self._make_url(endpoint), *args, **kwargs) as resp:
            ret = await resp.json()
            ret = self._format_resp(ret)
            if include_status:
                return self._dataclass(**ret["data"], status=resp.status)
            return self._dataclass(**ret["data"])

    async def get(self, endpoint: str, include_status: bool = False, *args, **kwargs) -> Dataclass:
        """GET an endpoint.

        Args:
            endpoint (str): The path to append to base url
            include_status (bool): Add status code to returned class

        Return:
            Dataclass from our request
        """
        async with self._client.get(self._make_url(endpoint), *args, **kwargs) as resp:
            ret = await resp.json()
            ret = self._format_resp(ret)
            if include_status:
                return self._dataclass(**ret["data"], status=resp.status)
            return self._dataclass(**ret["data"])

    async def delete(self, endpoint: str, include_status: bool = False, *args, **kwargs) -> None:
        """DELETE an endpoint.

        Args:
            endpoint (str): The path to append to base url
            include_status (bool): Add status code to returned class

        Return:
            None
        """
        async with self._client.delete(self._make_url(endpoint), *args, **kwargs) as resp:
            ret = await resp.json()
            if include_status:
                return self._dataclass(**ret["data"], status=resp.status)

    async def patch(self, endpoint: str, include_status: bool = False, *args, **kwargs) -> Dataclass:
        """PATCH an endpoint.

        Args:
            endpoint (str): The path to append to base url
            include_status (bool): Add status code to returned class

        Return:
            Dataclass from our request
        """
        async with self._client.patch(self._make_url(endpoint), *args, **kwargs) as resp:
            ret = await resp.json()
            ret = self._format_resp(ret)
            if include_status:
                return self._dataclass(**ret["data"], status=resp.status)
            return self._dataclass(**ret["data"])

    async def put(self, endpoint: str, include_status: bool = False, *args, **kwargs) -> Dataclass:
        """PUT an endpoint.

        Args:
            endpoint (str): The path to append to base url
            include_status (bool): Add status code to returned class

        Return:
            Dataclass from our request
        """
        async with self._client.put(self._make_url(endpoint), *args, **kwargs) as resp:
            ret = await resp.json()
            ret = self._format_resp(ret)
            if include_status:
                return self._dataclass(**ret["data"], status=resp.status)
            return self._dataclass(**ret["data"])


@dataclass(frozen=True)
class Root(Dataclass):
    """Definition for connections to our endpoints.

    Attributes:
        base_url (URL): Base host path for all of our connections
        show_traceback (bool): Show errors if they occur
        meta (Dict): Supply extra arguments to your `client` if not using default
        _client (Client): The client class to use (must at least subclass `Client`)

    Note:
        Because we define `default args`, this class CANNOT be subclassed easily.
        see: https://stackoverflow.com/a/53085935
    """

    base_url: URL
    show_traceback: bool
    meta: field(default_factory=dict) = None
    # Underscored attributes avoid type validation (useful for complex types and classes)
    _client: Client = Client

    def pprint(self):
        click.echo(f"base_url: {self.base_url}")
        click.echo(f"show_traceback: {self.show_traceback}")
        click.echo(f"_client: {self._client}")

    @asynccontextmanager
    async def client(self, *args, **kwargs) -> AsyncIterator[Client]:
        client = self._client(self.base_url, *args, **kwargs)
        try:
            yield client
        finally:
            await client.close()


def pass_root(f):
    """Creates passable object to click command.

    SeeAlso:
        https://click.palletsprojects.com/en/8.0.x/api/#click.make_pass_decorator
    """

    @click.pass_context
    def new_func(ctx, *args, **kwargs):
        obj = ctx.find_object(Root)
        try:
            return asyncio.run(ctx.invoke(f, obj, *args, **kwargs))
        except Exception as exc:
            if obj.show_traceback:
                raise
            else:
                click.echo(f"Error: {exc}")

    return functools.update_wrapper(new_func, f)
