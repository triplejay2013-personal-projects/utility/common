"""Implements python google api.

api-Reference: https://developers.google.com/sheets/api/quickstart/python

This requires the user to use a credential file for the service account. Make sure not to
include this file in version control. In production this should be provided via the environment
or some other means.
"""
import os
from pathlib import Path
from typing import Any, List

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

# The ID and range of a sample spreadsheet.
SAMPLE_SPREADSHEET_ID = "1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms"
SAMPLE_RANGE_NAME = "Class Data!A2:E"

# File Paths
CODE_PATH = Path(__file__).parent
HOMEDIR = os.path.expanduser("~/")


class GoogleApi:
    """Base class for Google Api access.

    Attributes:
        SCOPES (List(str)): Describes the scope of the users interaction with the API (readonly vs write)
    """

    SCOPES: List[str] = []  # Must be set by a subclass
    TOKEN_FILE = "token.json"
    CREDENTIAL_FILE = "credentials.json"

    def __init__(self, credential_path: str = HOMEDIR):
        """Uses local token.json to connect to Google API.

        your 'token.json' should NOT be placed into version control. This will need to
        be generated on a per case basis.
        """
        if self.SCOPES is None:
            raise ValueError(f"'{self.__class__.__name__}' must be initialized with SCOPES defined")
        self.creds: Any = None
        self.cp = Path(credential_path)
        self.load_creds()

    def load_creds(self):
        """Load credentials, or generate them if non-existant."""
        # The 'token.json' stores user access and refresh tokens, and is created automatically when the authorization
        # flow completes for the first time.
        if os.path.exists(self.cp / self.TOKEN_FILE):
            self.creds = Credentials.from_authorized_user_file(self.cp / self.TOKEN_FILE, self.SCOPES)
        # If there are no valid credentials available, let the user log in.
        if not self.creds or not self.creds.valid:
            if self.creds and self.creds.expired and self.creds.refresh_token:
                self.creds.refresh(Request())
            else:
                try:
                    flow = InstalledAppFlow.from_client_secrets_file(self.cp / self.CREDENTIAL_FILE, self.SCOPES)
                except FileNotFoundError:
                    print(
                        f"Must provide '{self.cp / self.CREDENTIAL_FILE}' auth for service api. "
                        "See https://developers.google.com/workspace/guides/create-credentials for details"
                    )
                    exit(1)
                self.creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open("token.json", "w") as token:
                token.write(self.creds.to_json())


class GoogleSheetsApi(GoogleApi):
    """Connect to Google Sheets API."""

    SCOPES = ["https://www.googleapis.com/auth/spreadsheets.readonly"]

    def __init__(self, spreadsheet_id: str, *args, **kwargs):
        """Create API pointed to specific sheet.

        Args:
            spreadsheet_id (str): The id of the spreadsheet (can be derived from URL)
        """
        super().__init__(*args, **kwargs)
        self.sheet_id = spreadsheet_id
        self.service = build("sheets", "v4", credentials=self.creds)

    def get(self, spreadsheet_range: str) -> Any:
        """Retrieve info from google sheets.

        Args:
            spreadsheet_range (str): The A1 notation to grab specific data from the sheet
        """
        try:
            sheet = self.service.spreadsheets()
            result = sheet.values().get(spreadsheetId=self.sheet_id, range=spreadsheet_range).execute()
            values = result.get("values", [])

            if not values:
                print("No data found.")
                return

            # This is a sample sheet, just print out some intersting info
            if self.sheet_id == SAMPLE_SPREADSHEET_ID:
                print("Name, Major:")
                for row in values:
                    # Print columns A and E, which correspond to indices 0 and 4.
                    print("%s, %s" % (row[0], row[4]))
            else:
                return values
        except HttpError as err:
            print(err)


if __name__ == "__main__":
    GoogleSheetsApi(SAMPLE_SPREADSHEET_ID).get(SAMPLE_RANGE_NAME)
