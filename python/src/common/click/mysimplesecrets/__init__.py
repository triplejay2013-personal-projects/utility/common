# type: ignore
# flake8: noqa
"""Simple file encryption/decryption."""

import os
from pathlib import Path

import click
from cryptography.fernet import Fernet

from common.fileop import preserve_original_file
from common.secrets import decrypt as dec
from common.secrets import encrypt as enc


@click.group()
def main():
    pass


def complete_path_names(ctx, param, incomplete):
    return [k for k in os.listdir() if k.startswith(incomplete)]


@main.command()
@click.argument("filename", type=click.File("wb"), default="key.key", shell_complete=complete_path_names)
def write_key_file(filename):
    """Generates a key and saves it to a file."""
    filename.write(Fernet.generate_key())


@main.command()
@click.argument("message", type=str)
@click.option("--key-file", type=str, default="key.key", shell_complete=complete_path_names)
def encrypt(message, key_file) -> bytes:
    """Encrypt a message."""
    click.secho(fg="cyan", message=enc(message, key_file))


@main.command()
@click.argument("secrets", type=str)
@click.option("--key-file", type=str, default="key.key", shell_complete=complete_path_names)
def decrypt(secrets, key_file) -> str:
    """Decrypts a message."""
    click.secho(fg="cyan", message=dec(secrets, key_file))


@main.command()
@click.argument("filename", type=click.Path(exists=True), shell_complete=complete_path_names)
@click.option("--key-file", type=str, default="key.key", shell_complete=complete_path_names)
@click.option("--out", is_flag=True, help="Outputs the name of the renamed file")
@click.option("--silent", is_flag=True, help="Disable error messages", default=False)
@click.option("--disable-check", is_flag=True, help="Disables file ext check", default=False)
@click.option("--disable-rename", is_flag=True, help="Disables file ext addition", default=False)
@click.pass_context
def encrypt_file(ctx, filename, key_file, out, silent, disable_check, disable_rename):
    """Encrypts a file with name FILENAME."""
    enc_name = filename
    path = Path(filename)
    if disable_check or path.suffix != ".enc":
        with preserve_original_file(filename, "r") as f:
            file_data = enc(f.read(), key_file)
        with preserve_original_file(filename, "wb") as f:
            f.write(file_data)
        if not disable_rename:
            enc_name = f"{filename}.enc"
            path.rename(enc_name)

        if out:
            click.echo(enc_name)
        ctx.exit(0)
    elif not silent:
        click.secho(
            f"Attempted to encrypt file not suffixed with 'enc': {filename}. Use '--disable-check' to ignore this",
            fg="red",
        )
    ctx.exit(1)


@main.command()
@click.argument("filename", type=click.Path(exists=True), shell_complete=complete_path_names)
@click.option("--key-file", type=str, default="key.key", shell_complete=complete_path_names)
@click.option("--out", is_flag=True, help="Outputs the name of the renamed file", default=False)
@click.option("--silent", is_flag=True, help="Disable error messages", default=False)
@click.option("--disable-check", is_flag=True, help="Disables file ext check", default=False)
@click.option("--disable-rename", is_flag=True, help="Disables file ext removal", default=False)
@click.pass_context
def decrypt_file(ctx, filename, key_file, out, silent, disable_check, disable_rename):
    """Decrypt a file with name FILENAME."""
    dec_name = filename
    path = Path(filename)
    if disable_check or path.suffix == ".enc":
        with preserve_original_file(filename, "rb") as f:
            decrypted_data = dec(f.read(), key_file)
        with preserve_original_file(filename, "w") as f:
            f.write(decrypted_data)
        if not disable_rename:
            name = filename.replace(".enc", "")
            path.rename(name)
            dec_name = f"{filename}.enc"
            dec_name = name

        if out:
            click.echo(dec_name)
        ctx.exit(0)
    elif not silent:
        click.secho(
            f"Attempted to encrypt file not suffixed with 'enc': {filename}. Use '--disable-check' to ignore this",
            fg="red",
        )
    ctx.exit(1)


if __name__ == "__main__":
    main()
