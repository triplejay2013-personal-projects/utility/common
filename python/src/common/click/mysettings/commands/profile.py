# type: ignore
# flake8: noqa
from pathlib import Path
from typing import Dict, List, Optional, Union
from uuid import uuid4

import click

from common.click.mysettings import DCONFDIR
from common.shell import Shell

shell = Shell()

PROFILE_DIR = f"{Path.home()}/.myprofiles"


def convert_type(value: str) -> Union[str, int]:
    """Try to intelligently convert type of VALUE."""
    for t in [int]:
        try:
            t(value)
        except ValueError:
            continue
        else:
            return t(value)
    return value


def get_profiles() -> List:
    """Get list of current profile ids."""
    return shell.run('gsettings get org.gnome.Terminal.ProfilesList list | tr -d "[]\',"')[0].split(" ")


def get_profile_name(profile_id: str) -> Optional[str]:
    """Get profile_name from given profile_id"""
    return shell.run(f"dconf read {DCONFDIR}/:{profile_id}/visible-name")[0].strip("'")


def get_profile_id(profile_name: str = "") -> Optional[str]:
    """Get profile_id from visible names in current profiles."""
    for p in get_profiles():
        name = get_profile_name(p)
        if name == profile_name:
            return p
    click.secho(f"No profile_id was found with profile name: {profile_name}")
    raise click.Abort()


def get_settings(profile_id: str) -> Dict[str, str]:
    keys = shell.run(f"dconf list {DCONFDIR}/:{profile_id}/")[0].split("\n")
    vals = []
    for key in keys:
        vals.append(shell.run(f"dconf read {DCONFDIR}/:{profile_id}/{key}")[0])
    return dict(zip(keys, vals))


@click.group()
@click.pass_context
def profile(ctx):
    """Customize and modify profiles."""


@profile.command()
def list():
    """List existing profile names and their ids."""
    for p in get_profiles():
        profile = get_profile_name(p)
        if not profile:
            profile = '""'
        click.echo(f"{profile}:{p}")


@profile.command()
@click.argument("profile_name")
def delete(profile_name):
    """Remove existing profile."""
    profile_id = get_profile_id(profile_name)
    cur_profiles = get_profiles()
    if len(cur_profiles) == 1:
        click.secho("Cannot have 0 profiles")
        return
    cur_profiles.remove(profile_id)
    shell.run(f'dconf write {DCONFDIR}/list "{cur_profiles}"')


@profile.command()
@click.argument("profile_name")
@click.option("--default/--no-default", default=True, show_default=True, help="Set new profile as default")
@click.option(
    "--ignore-existing",
    "ignore",
    is_flag=True,
    default=True,
    show_default=True,
    help="Ignore profile if the visible-name is already in use",
)
def create(profile_name: str, default: bool, ignore: bool):
    """Create new profile with PROFILE_NAME."""
    cur_profiles = get_profiles()
    if ignore:
        for p in cur_profiles:
            if get_profile_name(p) == profile_name:
                click.secho(
                    f"Profile Name '{profile_name}' already exists. (use --ignore-existing to ignore this check)",
                    fg="red",
                )
                return
    profile_id = str(uuid4())
    cur_profiles.append(profile_id)
    shell.run(f'dconf write {DCONFDIR}/list "{cur_profiles}"')
    if profile_name:
        shell.run(f"dconf write {DCONFDIR}/:{profile_id}/visible-name \"'{profile_name}'\"")
    else:
        shell.run(f'dconf write {DCONFDIR}/:{profile_id}/visible-name \\"\\"')
    if default:
        shell.run(f"dconf write {DCONFDIR}/default \"'{profile_id}'\"")


@profile.command()
@click.argument("profile_name", type=str, default="")
def get(profile_name: str):
    """Get settings for PROFILE_NAME."""
    profile_id = get_profile_id(profile_name)
    settings = get_settings(profile_id)
    for k, v in settings.items():
        click.echo(f"{k}={v}")


def set_profile_value(profile_id: str, key: str, default: str = None) -> None:
    """Set profile value."""
    try:
        show_default = default is not None
        default = default if default is not None else ""
        value = convert_type(click.prompt(key, default=default, show_default=show_default).strip("'"))
        if " " in value:
            value = f'"{value}"'
        if value:
            if value.lower() in ["true", "false"]:
                shell.run(f"dconf write {DCONFDIR}/:{profile_id}/{key} {value}")
            else:
                shell.run(f"dconf write {DCONFDIR}/:{profile_id}/{key} \\'{value}\\'")
    except KeyError:
        click.echo(f"Options: {list(settings.keys())}")
        return


@profile.command()
@click.argument("profile_name", type=str)
@click.option("--key", type=str, default=None, help="Change a specific key in the profile settings")
def set(profile_name: str, key: Optional[str]):
    """Set settings for PROFILE_NAME."""
    profile_id = get_profile_id(profile_name)
    settings = get_settings(profile_id)
    click.echo("Modify the following settings (press <enter> to leave default)")
    if key:
        set_profile_value(profile_id, key)
    else:
        for k, v in settings.items():
            set_profile_value(profile_id, k, default=v)


@profile.command()
@click.argument("cur", type=str)
@click.argument("new", type=str)
def rename(cur: str, new: str):
    """Rename profile_name CUR to NEW name. (renames first occurence)"""
    profile_id = get_profile_id(cur)
    shell.run(f"dconf write {DCONFDIR}/:{profile_id}/visible-name \\'{new}\\'")


@profile.command()
@click.argument("profile_name", type=str)
def dump(profile_name: str):
    """Dump dconf PROFILE settings to FILENAME. Use \"\" to use default unnamed"""
    profile_id = get_profile_id(profile_name)
    filename = f"{PROFILE_DIR}/{profile_name}.conf"
    shell.run(f"dconf dump {DCONFDIR}/:{profile_id}/ > {filename}")[0]


@profile.command()
@click.argument("profile_name", type=str)
def load(profile_name: str):
    """Load dconf PROFILE settings to FILENAME. Use \"\" to use default unnamed"""
    profile_id = get_profile_id(profile_name)
    filename = f"{PROFILE_DIR}/{profile_name}.conf"
    shell.run(f"dconf load {DCONFDIR}/:{profile_id}/ < {filename}")[0]


@profile.command()
@click.option("--filter", "fil", type=str, default="", help="Filter options by this string")
@click.option("--values/--no-values", default=True, show_default=True, help="Show existing values for each key")
def settings(fil: str, values: bool):
    """List global settings available for profiles on this device."""
    if values:
        cmd = "gsettings list-recursively org.gnome.Terminal.Legacy.Profile:/ | awk '{print $2 \" = \" $3}'"
    else:
        cmd = "gsettings list-recursively org.gnome.Terminal.Legacy.Profile:/ | awk '{print $2}'"
    settings = shell.run(cmd)[0].split("\n")
    settings.sort()
    for s in settings:
        if fil in s:
            click.echo(s)
