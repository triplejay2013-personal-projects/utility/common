# type: ignore
# flake8: noqa
"""mysettings utilizes `dconf` and `gsettings`. These tools aren't well documented
on how to use them the way I wanted to. This is a collection of methods that
interact with profiles and their settings.

https://encarsia.github.io/en/posts/gsettings/ --> Maybe can be used...

References:
    - Explanation of script used for impl: https://askubuntu.com/a/954592
    - Script used as base for impl: https://github.com/aruhier/gnome-terminal-colors-solarized/blob/master/src/profiles.sh
    - Values will be wrapped in '': https://developer.gnome.org/dconf/unstable/dconf-tool.html
    - How to set values with dconf: https://askubuntu.com/a/825780
    - Dump dconf settings: https://askubuntu.com/a/420589
"""
import os
from typing import Dict, List, Optional
from uuid import uuid4

import click

from common.shell import Shell

shell = Shell()

DCONFDIR = "/org/gnome/terminal/legacy/profiles:"


# https://click.palletsprojects.com/en/8.0.x/commands/#custom-multi-commands
plugin_folder = os.path.join(os.path.dirname(__file__), "commands")


class Main(click.MultiCommand):
    """Aggregate commands into single interface."""

    def list_commands(self, ctx):
        rv = []
        for filename in os.listdir(plugin_folder):
            if filename.endswith(".py") and filename != "__init__.py":
                rv.append(filename[:-3])
        rv.sort()
        return rv

    def get_command(self, ctx, name):
        ns = {}  # local/global dict
        fn = os.path.join(plugin_folder, name + ".py")
        with open(fn) as f:
            code = compile(f.read(), fn, "exec")
            eval(code, ns, ns)
        return ns[name]


@click.command(cls=Main)
@click.pass_context
def main(ctx):
    """Provide utilities to customize gnome-terminal."""


if __name__ == "__main__":
    main(obj={})
