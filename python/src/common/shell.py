# type: ignore
# flake8: noqa
"""Utility functions to run commands in the shell."""
import logging
import os
import pty
from contextlib import contextmanager
from pathlib import Path
from shlex import split as xsplit
from subprocess import PIPE, CalledProcessError, Popen
from typing import Callable, Dict, List, Optional, Tuple, Union

logger = logging.getLogger(__name__)


# TODO this class seriously needs some unit tests on simple bash functions and IO redirection and pipes


class Shell:
    """Provide custom shell execution utility."""

    def __init__(self, **kwargs):
        self.logger = kwargs.get("logger", logger)
        self.info = kwargs.get("loginfo", self.logger.info)
        self.debug = kwargs.get("logdebug", self.logger.debug)
        self.error = kwargs.get("logerror", self.logger.error)

        self.stdout = kwargs.get("stdout", False)
        self.stderr = kwargs.get("stderr", False)

    def _activate_helper_get_logging_vars(self) -> Tuple[Callable]:
        return self.info, self.debug, self.error

    def _activate_helper_set_logging_vars(self) -> None:
        # This only works because we pass similar args to both print and logging.info|error|debug
        def myprint(msg: Union[str, bytes]):
            if isinstance(msg, bytes):
                msg = msg.decode("utf-8")
            print(msg)

        self.info = myprint
        self.debug = myprint
        self.error = myprint

    def _activate_helper_reset_logging_vars(self, myvars: Tuple[Callable]) -> None:
        self.info = myvars[0]
        self.debug = myvars[1]
        self.error = myvars[2]

    @contextmanager
    def activate_stdout(self):
        """Set stdout to TRUE and PRINT to screen."""
        original_out = self.stdout
        logging_vars = self._activate_helper_get_logging_vars()
        try:
            self.stdout = True
            self._activate_helper_set_logging_vars()
            yield
        finally:
            self.stdout = original_out
            self._activate_helper_reset_logging_vars(logging_vars)

    @contextmanager
    def activate_stderr(self):
        """Set stdout to TRUE and PRINT to screen."""
        original_out = self.stderr
        logging_vars = self._activate_helper_get_logging_vars()
        try:
            self.stderr = True
            self._activate_helper_set_logging_vars()
            yield
        finally:
            self.stderr = original_out
            self._activate_helper_reset_logging_vars(logging_vars)

    @contextmanager
    def pushd(self, new_dir: Union[str, Path, None]) -> None:
        """change directory context within context manager."""
        if new_dir is None:
            yield  # Do nothing and return
            return
        # Otherwise change dirs
        prev_cwd = Path.cwd()
        os.chdir(new_dir)
        try:
            yield
        finally:
            os.chdir(prev_cwd)

    def interactive(
        self, command: str = "/bin/bash", read: Callable = None, cwd: Union[str, Path, None] = None
    ) -> None:
        """Provide interactive shell for COMMAND.

        Args:
            command (str): The command to initialize session with. It is expected
                that this command provides tty to interact with.
            read (Callable): stdin_read passed to pty.spawn()
            cwd (Union[str, Path, None]): Directory in which we want to run `command`
        """
        if isinstance(command, str):
            command = xsplit(command)
        with self.pushd(cwd):
            # Launch pseudo terminal (interactive commands)
            if read is None:
                pty.spawn(command)
            else:
                pty.spawn(command, read)

    def parse_pipes(self, command: List) -> List[List]:
        """Parse command for pipe args and split into sub-lists.

        Args:
            command (List): The command to pass to split on '|'

        Returns
            List of command lists to format for piping
        """
        pipes = []
        cur = 0
        for index in [i for i in range(len(command)) if command[i] == "|"]:
            pipes.append(command[cur:index])
            cur = index + 1
        # Add last (or only) command to the list
        pipes.append(command[cur:])
        return pipes

    def run(
        self, command: Union[str, List], msg: str = "", cwd: Union[str, Path, None] = None, env: Optional[Dict] = None
    ) -> Tuple[str]:
        """Run an installation instruction in the shell.

        Supports a pipe operations (just include "|" in command). This avoids the
        Popen 'shell' option for safe operation

        Supports IO redirection to and from files (just include "<" or ">" in command)
        NOTE: This will not work with redirection of another cmd's output/input yet
        NOTE: Seriously, ONLY USE WITH FILES

        Args:
            command (List, str): Passed directly to subprocess. List of args, or
                space-delimitted command description
            msg (str): Display before running command
            cwd (Union[str, Path, None]): Directory in which we want to run `command`

        Returns:
            Tuple of str (stdout, stderr)

        Raises:
            CalledProcessError, TimeExpired on failure
        """
        self.info(msg)
        if isinstance(command, str):
            command = xsplit(command)
        # Remove empty args (they can break Popen)
        command = [c for c in command if c]
        commands = self.parse_pipes(command)

        cmd, myoutput = self.myoutput(commands[0])
        if myoutput != PIPE:
            # This assumes there will be no pipes if the first command has output redirects
            ps = Popen(cmd, stdout=myoutput, stderr=PIPE, cwd=cwd, env=env)
        else:
            cmd, myinput = self.myinput(commands[0])
            ps = Popen(cmd, stdin=myinput, stdout=PIPE, stderr=PIPE, cwd=cwd, env=env)
            if len(commands) > 1:
                for i in range(1, len(commands)):
                    cmd, myoutput = self.myoutput(commands[i])
                    ps = Popen(cmd, stdin=ps.stdout, stdout=myoutput, stderr=PIPE, cwd=cwd, env=env)

        out, err = ps.communicate()

        # IO redirection, these are only set if PIPE was used
        out = b"" if out is None else out
        err = b"" if err is None else err

        if self.stdout:
            self.info(out)
        if self.stderr:
            self.error(err)
        if ps.returncode:
            self.debug(err)
            raise CalledProcessError(ps.returncode, command)
        return out.decode("utf-8").strip("\n"), err.decode("utf-8").strip("\n")

    @staticmethod
    def myoutput(command: List) -> Tuple:
        """Provided a command, supply stdout for Popen.

        Args:
            command (List): The command to analyze

        Returns:
            Tuple:
                Command to execute (removes ">")
                File descriptor for stdout
        """
        try:
            i = command.index(">")
        except ValueError:
            pass
        else:
            args = command[:i]
            name = command[i + 1 :][0]  # +1 ignore > symbol
            # TODO handle command redirection as well
            return args, open(name, "w+")
        return command, PIPE

    @staticmethod
    def myinput(command: List) -> Tuple:
        """Provided a command, supply stdin for Popen.

        Args:
            command (List): The command to analyze

        Returns:
            Tuple:
                Command to execute (removes "<")
                File descriptor for stdin (or None)
        """
        try:
            i = command.index("<")
        except ValueError:
            pass
        else:
            args = command[:i]
            name = command[i + 1 :][0]  # +1 ignore < symbol
            # TODO handle command redirection as well
            return args, open(name, "r")
        return command, None
