# type: ignore
# flake8: noqa
"""File Operations utility."""
import os
from contextlib import contextmanager
from pathlib import Path


def load_file(filename: str) -> Path:
    """Locate and load a file into a Path object.

    `filename` may include a path with the filename. All paths are considered
    relative to the programs CWD

    Args:
        filename (str): The file to load
    """
    if not filename:
        raise ValueError("Please specify a file")
    dirpath = str(Path.cwd()) + filename if filename.startswith("/") else str(Path.cwd()) + "/" + filename
    path = Path(dirpath)
    if not path.exists():
        raise ValueError("File not found '%s'" % str(path))
    return path


@contextmanager
def preserve_original_file(*args, **kwargs):
    """Open file and mutate it safely.

    Params:
        args & kwargs: Anything you would pass to `open` you can pass here

    Returns:
        yields the current File object

    Raises:
        The initial exception being handled (if any)

    Notes:
        This helps preserve the original content of the file, should anything go
        wrong whilst handling / mutating file data.
    """
    # file is expected at args[0] (no defaults; required)
    filename = args[0]
    orig_data = ""
    try:
        with open(*args, **kwargs) as f:
            yield f
    except Exception as exc:
        if orig_data:
            with open(args[0], mode="w") as f:
                f.write(orig_data)
        exc.args = (f"Unable to encrypt file '{filename}': {exc}",)
        raise


@contextmanager
def pushd(path: str):
    """Push into working directory."""
    oldcwd = os.getcwd()
    os.chdir(path)
    try:
        yield  # pushd
    finally:  # popd
        os.chdir(oldcwd)


def expand_path(path: str):
    """Expands user vars in path string."""
    newpath = path
    if "$HOME" in path:
        newpath = newpath.replace("$HOME", str(Path.home()))
    if "~" in path:
        newpath = newpath.replace("~", str(Path.home()))
    return newpath
