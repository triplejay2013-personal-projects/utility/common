"""Process PIL Images or mss Screenshots."""
import logging
import time
from typing import Dict, Tuple, Union

import mss
from PIL import Image

logger = logging.getLogger(__name__)


class Color:
    # Pulled from https://www.ginifab.com/feeds/pms/color_picker_from_image.php
    LIGHT_YELLOW = (255, 210, 0)
    YELLOW = (255, 253, 0)
    WHITE = (255, 255, 255)
    LIGHT_GREEN = (78, 220, 0)
    DARK_ORANGE = (241, 60, 11)
    PURPLE = (204, 117, 255)
    LIGHT_BLUE = (0, 198, 255)
    RED = (255, 29, 0)


class OcrImageProcessor:
    """Prepare images to be fed into OCR object.

    Notes:
        This process includes homogenizing text colors, and inverting text (by color provided by the caller),
        and removing all other colors from the image. The black text and white background is fed into
        the OCR tesserocr api, and processed for text.
    """

    THRESHOLD = 30

    @staticmethod
    def luminance(pixel: Tuple[int, int, int]) -> float:
        """Uses math and science to determine the brightness of a color."""
        # https://stackoverflow.com/a/26768008
        return 0.299 * pixel[0] + 0.587 * pixel[1] + 0.114 * pixel[2]

    # https://stackoverflow.com/questions/596216/formula-to-determine-perceived-brightness-of-rgb-color/596243#596243
    @classmethod
    def is_similar(cls, pixel_a, pixel_b, threshold) -> bool:
        """Compares the luminance of two pixels."""
        return abs(cls.luminance(pixel_a) - cls.luminance(pixel_b)) < threshold

    @classmethod
    def invert(cls, img: Image, color: Tuple[int, int, int], threshold: int = None, tolerance: int = None):
        """Invert an image, preserving <color> as black, and everything else as white."""
        for i in color:
            if i not in range(0, 256):
                raise ValueError(f"Invalid RGB value: {i}")

        tolerance = tolerance if tolerance else 30  # Homogenize a pixel value within <tolerance> units of rgb
        width, height = img.size

        WHITE = 255
        BLACK = 0
        PLACEHOLDER = -27

        start = time.time()
        width, height = img.size
        pixels = img.load()
        for x in range(width):
            for y in range(height):
                # Make pixel similar to chosen color value (less variance in surround colors)
                if cls.is_similar(pixels[x, y], color, threshold if threshold else cls.THRESHOLD):
                    pixels[x, y] = color
                # Make <color> black, and everything else white
                r, g, b, *alpha = pixels[x, y]
                r = PLACEHOLDER if abs(color[0] - r) < tolerance else r
                g = PLACEHOLDER if abs(color[1] - g) < tolerance else g
                b = PLACEHOLDER if abs(color[2] - b) < tolerance else b
                if (r, g, b) == (PLACEHOLDER, PLACEHOLDER, PLACEHOLDER):
                    pixels[x, y] = (BLACK, BLACK, BLACK)
                else:
                    pixels[x, y] = (WHITE, WHITE, WHITE)
        logger.trace("Image inversion took %ss", time.time() - start)  # type: ignore[attr-defined]
        return img

    @classmethod
    def make_pil_image(cls, sct_img) -> Image:
        """Make PIL.Image from mss.Screenshot."""
        start = time.time()
        img = Image.new("RGB", sct_img.size)
        pixels = zip(sct_img.raw[2::4], sct_img.raw[1::4], sct_img.raw[0::4])
        img.putdata(list(pixels))
        logger.trace(  # type: ignore[attr-defined]
            "Converting mss.Screenshot to PIL.Image took %ss", time.time() - start
        )
        return img

    @classmethod
    def screenshot(cls, box: Union[Dict, Tuple]) -> Image:
        """Screenshot part of the desired screen.

        Args:
            box (Dict): where to snap the screenshot (format: {
                    "left": x-coordinate of top-left corner,
                    "top": y-coordinate of top-left corner,
                    "width": width from top-left corner,
                    "height": height from top-left corner,
                }). A Tuple may also be accepted, see mss.grab documentation for details
        """
        start = time.time()
        with mss.mss() as sct:
            try:
                sct_img = sct.grab(box)
            except mss.ScreenShotError as e:
                logger.exception(e)
                raise e
        logger.trace("mss.Screenshot took %ss", time.time() - start)  # type: ignore[attr-defined]
        return cls.make_pil_image(sct_img)
