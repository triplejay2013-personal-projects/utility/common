import logging
import time
from typing import Union

from PIL import Image
from tesserocr import PSM, PyTessBaseAPI

from common.machine_learning.ocr.config import TESSDATA

logger = logging.getLogger(__name__)


class OCR:
    """Base tesseract api class."""

    def __init__(self, psm: Union[int, str], whitelist: str = "", **kwargs):
        """Base OCR wrapper for the tesserocr wrapper around tesseract-api.

        Args:
            psm (int,str): Page Segmentation Model for this object (see below)
            whitelist (str): Characters to look for in addition to the specified request (numbers, alpha, etc.)
            kwargs: Passed directly to PyTessBaseAPI

        Notes:
            The reason we use tesserocr over pytesseract, is pytesseract loads the model on each invocation, which
            is rather slow (at time of writing, about 0.5s for a small single word image of text). tesserocr loads
            the model once, and reuses the API, this gives significant speedup in my use case, allowing us to process
            a small single word image in less than 0.02 seconds (at time of writing this comment)

        Taken from: https://github.com/sirfz/tesserocr/blob/master/tesserocr.pyx#L102
        search 'cdef class PSM(_Enum):' if the line number doesn't match
        PSM:
            OSD_ONLY: Orientation and script detection only.
            AUTO_OSD: Automatic page segmentation with orientation and script detection. (OSD)
            AUTO_ONLY: Automatic page segmentation, but no OSD, or OCR.
            AUTO: Fully automatic page segmentation, but no OSD. (:mod:`tesserocr` default)
            SINGLE_COLUMN: Assume a single column of text of variable sizes.
            SINGLE_BLOCK_VERT_TEXT: Assume a single uniform block of vertically aligned text.
            SINGLE_BLOCK: Assume a single uniform block of text.
            SINGLE_LINE: Treat the image as a single text line.
            SINGLE_WORD: Treat the image as a single word.
            CIRCLE_WORD: Treat the image as a single word in a circle.
            SINGLE_CHAR: Treat the image as a single character.
            SPARSE_TEXT: Find as much text as possible in no particular order.
            SPARSE_TEXT_OSD: Sparse text with orientation and script det.
            RAW_LINE: Treat the image as a single text line, bypassing hacks that are Tesseract-specific.
            COUNT: Number of enum entries.
        """
        PSM_VALS = {p: getattr(PSM, p) for p in dir(PSM) if p.isupper()}
        if isinstance(psm, str):
            self.psm = PSM_VALS.get(psm)
            if not isinstance(self.psm, int):
                raise ValueError(f"{psm} is not a valid PSM value. Valid values are: {PSM_VALS}")
        elif isinstance(psm, int):
            self.psm = psm
        else:
            raise ValueError(f"'psm' must be a string or tesserocr.PSM.int object not {type(psm)}")
        self.api = PyTessBaseAPI(path=str(TESSDATA), psm=self.psm, **kwargs)
        # self.api.SetVariable("tessedit_char_whitelist", whitelist)
        self.whitelist = whitelist

    def __del__(self):
        if hasattr(self, "api") and self.api:
            self.api.End()

    def only_alphabet(self, string: str):
        return "".join([val.replace("\n", "") for val in string if val.isalnum() or val in self.whitelist])

    def only_number(self, string: str):
        return "".join([val for val in string if val.isnumeric() or val in self.whitelist])

    def image_to_str(self, img: Image) -> str:
        start = time.time()
        self.api.SetImage(img)
        val = self.only_alphabet(self.api.GetUTF8Text()).lower()
        logger.trace("image_to_str took %ss", time.time() - start)  # type: ignore[attr-defined]
        return val

    def image_to_int(self, img: Image) -> int:
        """Convert PIL Image to string."""
        start = time.time()
        self.api.SetImage(img)
        val = int(self.only_number(self.api.GetUTF8Text()))
        logger.trace("image_to_int took %ss", time.time() - start)  # type: ignore[attr-defined]
        return val
