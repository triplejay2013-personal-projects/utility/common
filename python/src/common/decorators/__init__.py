# type: ignore
# flake8: noqa
"""Define common decorators."""


def classproperty(f):
    """Similar to python 'property' descriptor.

    Notes:
        This only implements __get__, if you need more instantiate a class and use
        property like a normal person :-)
    """

    class DataDescriptor:
        def __get__(self, instance, icls):
            return f(icls)

    return DataDescriptor()
