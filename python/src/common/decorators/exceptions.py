# type: ignore
# flake8: noqa
from functools import wraps
from typing import Callable, Tuple


def exception_wrapper(exceptions: Tuple[Exception], handlers: Tuple[Callable] = None):
    """Wrap a method in try/except block using EXCEPTIONS.

    Args:
        exceptions (Tuple): Tuple of Exceptions to catch
        handlers (Tuple): (optional) Tuple of callbacks to execute if a correlated exception is caught

    Notes:
        this effectively one-lines the following:
            try:
                ...
            except exception1:
                handler1
            except exception2:
                handler2

        Handlers are excpected to map 1-to-1 with the Tuple of exceptiosn. Ie handlers[1] -> exceptions[1]
        If you don't want a handler, simply pass None

    Examples:
        @exception_wrapper((ValueError, OSError), (None, lambda: 3))
    """
    if not isinstance(exceptions, Tuple):
        raise ValueError(f"Unexpected types for `exceptions` '{type(exceptions)}'")
    if handlers and not isinstance(handlers, Tuple):
        raise ValueError(f"Unexpected types for `handlers` '{type(handlers)}'")
    if handlers and not len(handlers) == len(exceptions):
        raise ValueError(
            f"Expected len of `exceptions` and `handlers` to be the same {len(exceptions)} != {len(handlers)}"
        )

    def _decorator(func):
        """Defines our decorator."""

        @wraps(func)
        def _call(*args, **kwargs):
            """Defines the call to wrapped func."""
            try:
                func(*args, **kwargs)
            except exceptions as exc:
                for i in range(len(handlers)):
                    if exceptions[i]().__class__.__name__ == exc.__class__.__name__:
                        handler = handlers[i]
                        if handler:
                            handler()  # TODO args/kwargs?

        return _call

    return _decorator


@exception_wrapper((ArithmeticError, FloatingPointError), (None, lambda: 3))
def _test():
    raise ArithmeticError()


if __name__ == "__main__":
    _test()
