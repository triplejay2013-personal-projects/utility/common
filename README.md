# common

Common methods and code I might need to reuse.

Cool ways to do things in a variety of languages etc.

If ever I find myself re-writing a piece of code for multiple projects, then it belongs
here.

## Python

Python common functionalities, including (but not limited to):
- async operations
- async Click
- Custom shell implementation
- logging
- OCR / ASR implementations (TODO)
