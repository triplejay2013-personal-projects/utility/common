#!/bin/bash
GIT_ROOT=`git rev-parse --show-toplevel`
H_VENV=.hooks_venv
PRE_COMMIT_VER=2.20.0

# Force reinstall of existing deps
FORCE=${FORCE:-false}

pushd $GIT_ROOT > /dev/null
# Clear out existing hooks
if [ ! -d $GIT_ROOT/$H_VENV ] || [ ! -f $GIT_ROOT/.git/hooks/pre-commit ] || [ "$FORCE" = true ]; then
  rm -rf $GIT_ROOT/.git/hooks/*
  rm -rf $GIT_ROOT/$H_VENV
  python3 -m venv $H_VENV
  . $H_VENV/bin/activate
  if [ ! -f $GIT_ROOT/.pre-commit-requirements.txt ]; then
    echo "pre-commit==$PRE_COMMIT_VER" > .pre-commit-requirements.in
    pip3 install pip-tools
    pip-compile --allow-unsafe --no-emit-index-url .pre-commit-requirements.in
  fi
  pip3 install -r $GIT_ROOT/.pre-commit-requirements.txt
  if [ -f dev-requirements.txt ]; then
    pip3 install -r dev-requirements.txt
  fi

  pre-commit install
  pre-commit autoupdate
  pre-commit run --all-files
  deactivate
else
  echo "Use FORCE=true $0 to update hooks"
  . $H_VENV/bin/activate
  pre-commit run --all-files
  deactivate
fi
popd > /dev/null
