@REM https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/windows-commands
@ECHO off
IF EXIST .hooks_venv RD /s /q .hooks_venv
IF EXIST .git/hooks/pre-commit DEL /q .git\hooks\pre-commit
@ECHO on
python3 -m venv .hooks_venv
call .hooks_venv/Scripts/activate.bat
IF NOT EXIST .pre-commit-requirements.txt call pip3 install pip-tools
IF NOT EXIST .pre-commit-requiements.txt call pip-compile --allow-unsafe --no-emit-index-url .pre-commit-requirements.in
pip3 install -r .pre-commit-requirements.txt
pip3 install -r dev-requirements.txt
pre-commit install
pre-commit autoupdate
pre-commit run --all-files
call .hooks_venv/Scripts/deactivate.bat