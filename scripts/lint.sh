#!/bin/bash
APP_DIR=$(dirname $(dirname $(readlink -f "${BASH_SOURCE[0]}")))
HOOK_VENV=${HOOK_VENV:-$APP_DIR/.hooks_venv}

pushd $APP_DIR > /dev/null
if [ ! -d .hooks_venv ]; then
    echo "Could not find .hooks_venv"
    exit 1
fi
. $HOOK_VENV/bin/activate && pre-commit run --all-files && deactivate
popd > /dev/null
